package com.example.activitylogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityLogin extends AppCompatActivity {

    List<Usuario> list = new ArrayList<Usuario>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnLoginIniciar = (Button) findViewById(R.id.btnLoginIniciar);

        btnLoginIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edtUsuarioIniciar = (EditText) findViewById(R.id.edtUsuarioIniciar);
                EditText edtPasswordIniciar = (EditText) findViewById(R.id.edtPasswordIniciar);

                String usuario_extra = getIntent().getExtras().getString("usuario_registro_extra");
                String nombre_extra = getIntent().getExtras().getString("nombre_registro_extra");
                String edad_extra = getIntent().getExtras().getString("edad_registro_extra");
                String telefono_extra = getIntent().getExtras().getString("telefono_registro_extra");
                String descripcion_extra = getIntent().getExtras().getString("descripcion_extra");
                String password_extra = getIntent().getExtras().getString("password_registro_extra");

                // Se obtienen valores del putExtra que fueron enviados del activity Registro


                Usuario usuario1 = new Usuario(usuario_extra,nombre_extra,edad_extra,telefono_extra,descripcion_extra,password_extra);
                list.add(usuario1);
                Registro registro = new Registro(list);

                for(Usuario datos : registro.getUsuario()){
                    if(edtUsuarioIniciar.getText().toString().isEmpty() || edtPasswordIniciar.getText().toString().isEmpty()){
                        Toast.makeText(ActivityLogin.this,  "¡Error! Usuario o Password Vacio", Toast.LENGTH_SHORT).show();
                    }else if(edtUsuarioIniciar.getText().toString().equals(datos.getUsuario()) && edtPasswordIniciar.getText().toString().equals(datos.getPassword())){
                        // Si el campo de usuario y password es igual al que se registro llevara a la siguiente pantalla

                        Intent i = new Intent(v.getContext(), ActivityPerfil.class);
                        i.putExtra("usuario_registro_extra_l", usuario_extra);
                        i.putExtra("nombre_registro_extra_l", nombre_extra);
                        i.putExtra("edad_registro_extra_l", edad_extra);
                        i.putExtra("telefono_registro_extra_l", telefono_extra);
                        i.putExtra("descripcion_registro_extra_l", descripcion_extra);
                        i.putExtra("password_registro_extra_l", password_extra);
                        startActivity(i);

                    }else{
                        // De lo contrario mostrara un error que el usuario p password es incorrecto o no corresponde con el registro
                        Toast.makeText(ActivityLogin.this,  "¡Error! Usuario o Password incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}